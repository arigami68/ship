var prompt1 = function() {
    var num = 1;
    var promp = $('#prompt1 div');
    promp.hide();

    var lineContents = new Array();

    var terminal = function () {

        var skip = 0;
        var typeLine = function (idx) {
            idx == null && (idx = 0);

            var element = promp.eq(idx);
            var content = lineContents[idx];
            if (typeof content == "undefined") {
                $('.skip1').hide();
                return;
            }
            var charIdx = 0;

            var typeChar = function () {
                var rand = Math.round(Math.random() * 150) + 55;
                setTimeout(function () {
                    var char = content[charIdx++];

                    element.append(char);

                    if (typeof char !== "undefined")
                        typeChar();
                    else {
                        element.removeClass('active1');
                        typeLine(++idx);
                    }
                }, skip ? 0 : rand);
            }
            content = '' + content + '';
            element.append(' ').addClass('active1');

            typeChar();

        }

        promp.each(function (i) {
            lineContents[i] = $(this).text();
            $(this).text('').show();
        });

        typeLine();
    }

    terminal();
}
var prompt2 = function() {
    var num = 2;
    var lines = $('#prompt2 > div');
    lines.hide();
    var lineContents = new Array();



    var terminal = function () {

        var skip = 0;
        var typeLine = function (idx) {
            idx == null && (idx = 0);
            var element = lines.eq(idx);

            var content = lineContents[idx];
            if (typeof content == "undefined") {
                $('.skip'+num).hide();
                return;
            }

            var charIdx = 0;

            var typeChar = function () {
                var rand = Math.round(Math.random() * 150) + 55;
                setTimeout(function () {

                    var char = content[charIdx++];
                    element.append(char);
                    if (typeof char !== "undefined")
                        typeChar();
                    else {
                        element.removeClass('active'+num);
                        typeLine(++idx);
                    }
                }, skip ? 0 : rand);
            }
            content = '' + content + '';

            element.append(' ').addClass('active'+num);
            typeChar();
        }

        lines.each(function (i) {
            lineContents[i] = $(this).text();
            $(this).text('').show();
        });

        typeLine();
    }
    terminal();
}