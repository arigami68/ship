function Timer(){
    this.sTimer = new Date();
    this.text;
    this.time = function(){
        var current= new Date();
        var l = Date.parse(this.sTimer)/1000;
        var n = Date.parse(current)/1000;
        var r = n - l;
        var c = new Date('Thu, 01 Jan 1970 00:00:00');
        c.setSeconds(r);

        var h = c.getHours() ? ('0' + (c.getHours())).slice(-2)+':' : '';
        var m = c.getMinutes() ? ('0' + (c.getMinutes())).slice(-2)+':' : '';
        var s = ('0' + (c.getSeconds())).slice(-2)+':';
        var mili = current.getMilliseconds();
        var string = h+m+s+mili;

        this.text = string;
    };
    this.update = function(){
        this.time();
        var t = document.getElementById('timer')
        t.innerHTML = this.text;
    };
}

var heartShip = {
    map : {},
    log : {},
    color: {
        value : 20,
        colors : {},
        obj : {
            cyan : 'Красный',
            yellow : 'Желтый'
        },
        all    : [
            '#info1',
            '#info2'
        ]
    },
    timeStart : {},
    timeWay : {},
    obj : ['cyan', 'yellow'],
    ship : {
        status : {},
        value : {
            0 : 'Отсутсвует',
            1 : 'В движении',
            2 : 'На месте',
        },
        zone : {},
        move : {},
        aSpeed : 0,
        active : {}
    }
};
heartShip.class = new UpdateColors();
heartShip.log = new LogEvent();
function Ship(){
    this.time = function(name){
        var current= new Date();
        var l = Date.parse(heartShip.timeStart[name] ? heartShip.timeStart[name] : current)/1000;
        var n = Date.parse(current)/1000;
        var r = n - l;
        var c = new Date('Thu, 01 Jan 1970 00:00:00');
        c.setSeconds(r);

        var h = c.getHours() ? ('0' + (c.getHours())).slice(-2)+'h:' : '';
        var m = c.getMinutes() ? ('0' + (c.getMinutes())).slice(-2)+'m:' : '';
        var s = ('0' + (c.getSeconds())).slice(-2)+'s';
        var string = h+m+s;

        heartShip.timeWay[name] = string;
    };
    this.x          = 0;
    this.y          = 0,
    this.fillX      = 0,
    this.fillY      = 0,
    this.width      = 0,
    this.height     = 0,
    this.widthLine  = 20,
    this.widthIndent= 5,

    this.coord = function(rect){
        this.x = rect.x;
        this.y = rect.y;
        var name = rect.color;
        if(heartShip.timeStart[name] === undefined){ // Время запуска
            var date = new Date();

            heartShip.timeStart[name] = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2) + ' ' + date.getHours() + ':' + date.getMinutes()  + ':'  + date.getSeconds();
        }
        this.time(name);
        this.fillX = this.x - this.widthLine - this.widthIndent - 10 - 70;
        this.fillY = this.y - 50;
        this.width = 70;
        this.height = 40;
    },
    this.stat = function(){
        heartShip.obj.forEach(function(value){
            heartShip.ship.status[value] = heartShip.ship.value[0];
        });

    }

};
/**
 *
 * @constructor
 * @comment :: действия над событиями
 */
function LogEvent(){
    this.array = new CollectionObj();
    this.timeRange = 1;
    this.colors = {
        all : ['warning', 'danger'],
        flag : 1
    }
    this.add = function(obj){
        var self = this;
        var flag = 1;


        this.array.pull().forEach(function(name, value){
            if(obj.name == name.name){
                var time = obj.interval ? obj.interval : self.timeRange;
                flag = (obj.time > (name.time + time)) ? 1 : 0;
            }
        });

        if(!flag) return false;


        this.array.push(obj);
        this.push(obj.text, $('.log').find('table'));
    };
    this.push = function(obj, log){
        this.colors.flag^=1;
        if(!obj.import) {
            obj.import = this.colors.all[this.colors.flag];
        }
        if(log.find('tr').length > 12){ $('.log').find('table').find('tr').eq(-1).remove()}
        log.prepend('<tr class="'+obj.import+'">'+
        '<td class="time">'+obj.time+'</td>'+
        '<td class="value">'+obj.name+'</td>'+
        '</tr>');
    }
}
/*
 @name :: UpdateColors
 @comment :: Add global obj to event
 */
function UpdateColors(){
    heartShip.obj.forEach(function(color){
        (heartShip.color.colors[color] === undefined) ? ( heartShip.color.colors[color] = []) : null;
    });
    this.push   = function(data, color){
        var h = heartShip;

        if(h.color.colors[color].length > h.color.value){
            h.color.colors[color].shift();
        }
        h.color.colors[color].push (data);
    };
    this.pull   = function(color){ return heartShip.color.colors[color];}
}

function Preload(){
    this.active = [];
    this.reset();
    this.init = function(rect){
        var flag = true;
        this.pull().forEach(function(name) {
            if(name.color == rect.color) flag = false;
        });
        rect.date = new Date();
        rect.width = rect.height = 50;
        rect.x = Math.round(rect.x*2); // *1.31
        rect.y = Math.round(rect.y*2); // *1.05
        if(flag){
            this.active.push(rect);
            this.push(rect);
            heartShip.class.push(rect, rect.color);
        }
        return flag;
    }
}
inherit(Preload, CollectionObj, CollectionObj);
function CollectionObj(){
    var array = [];
    this.push   = function(data){array.push(data);}
    this.pull   = function(){ return array;}
    this.reset  = function(){ array = [];}
};
function Collection(){
    this._arrayCollect = [];

    this.push   = function(data){for(var value in data){this._arrayCollect.push(data[value]);}};
    this.pull   = function(){ return this._arrayCollect;};
    this.reset  = function(){ this._arrayCollect = [];}
};

function c1(){
    this.call = 'cyan'
    this.name = 'Красный',
    this.short = 'ММ'
    this.render = {
        ship : {
            ship : $('#cSpinner1'),
            value : 1,
        },
        prompt : {
            prompt  : $('#info1'),
            data    : $('#promptData1'),
            func    : [
                prompt1,prompt2
            ],
            all    : [
                $('#info1'),
                $('#info2')
            ],
        }
    }
    this.color = function(r, g, b) {

        if ((r-g) >= 50 &&  ((r-b)>50) && r>100){
            return true;
        }
/*
        if (r>100 && b>70 && g>70 && r+g+b < 300 ){
            return true;
        }
        */
    };

    this.colorShip = '#ff0000';
    this.colorShipRGB = [255, 0, 0];
};
function c2(){
    this.call = 'yellow'
    this.name = 'Желтый'
    this.short = 'Нос'
    this.colorShip = '#00ff00';
    this.colorShipRGB = [255, 255, 0];
    this.render = {
        ship : {
            ship : $('#cSpinner2'),
            value : 1,
        },
        prompt : {
            prompt  : $('#info2'),
            data    : $('#promptData2'),
            func    : [
                prompt1,prompt2
            ],
            all    : [
                $('#info1'),
                $('#info2')
            ],
        }
    }
};

function Canvas(obj){
    this.width;
    this.height;
    this.canvas;
    this.context;
    this.ship; // Работаем с кораблем
    this.css = obj;

    this.init = function(){
        var input = this.css.input;


        if(true){
            var glow =  input.glow.main;
            glow.height = input.glow.height;
            glow.width = input.glow.width;
        }
        if(true) {


            input.up.css({
                'height'    : input.maps.hp+'%',
                'width'     : input.maps.wp+'%',
            });

            input.maps.main.width  = input.up.width();
            input.maps.main.height = input.up.height();

            input.video.main.css({
                'height'    : input.up.height()/2+'px',
                'width'     : input.up.width()/2+'px'
            });
            this.width = input.up.width();
            this.height = input.up.height();
            this.canvas = this.context = document.getElementById('maps').getContext('2d');
        }



    }
};


/**
 * @name    :: Game
 * @comment :: Точка входа
 */
function Game(){
    this.elements       = [];
    this.preElements    = [];
    this.canvas;
    this.preload;
    this.ship;
    this.getElemets = function(sw){
        return (sw === 'preInit') ? this.preElements : this.elements;
    }
    this.push = function(obj, sw){
        var el = this.getElemets(sw);
        var flag = 1;

        for(var t in el){
            if(obj.getName() == el[t].getName()) flag=0;
        }
        flag ? el.push(obj) : null;
    };
    this.init = function(){
        var el = this.elements;
        for(var t in el){
            el[t].__proto__ = this;
            el[t].init();
        }
    };
    this.preInit = function(event){
        var el = this.preElements;
        for(var t in el){
            el[t].__proto__ = this;
            el[t].init(event);
        }
    }
    /**
     *
     * @todo Для каждого нужно было написать свой метод delete
     */
    this.delete = function(name){
        var el = this.elements;
        for(var t in el){


            if(this.elements[t].getName() == 'dropdownInfo'){
                heartShip.color.all.forEach(function(value){
                    $(value).hide();
                });
            }else if(this.elements[t].getName() == 'position'){

                $('#zone').hide();
                $('#container').hide();
                $('#view-game').css('backgroundImage', 'url(img/v-on.png)');
                $('#view-game').attr('flag', 'off');
            }else if(this.elements[t].getName() == 'winner') {
                $('.winner').hide();
            }
            if(this.elements[t].getName() == name) {
                this.elements.splice(t,1);
            }

        }
    }
}
/**
 * @name    :: GameGrid
 * @comment :: Сетка на корабли
 */
function GameGrid(){
    this.dataGrid =  {
        width  : 300,
        height : 300,
    };
    this.getName = function(){return 'grid'};
    this.init = function(){

        var context = this.canvas.context;

        this.dataGrid.width = this.canvas.height/4;
        this.dataGrid.height = this.canvas.width/5;

        var x = this.ship.x;
        var y = this.ship.y;

        var opacity = this.dataGrid + {
            x : Math.round((x-this.canvas.height/4)/this.canvas.height/4)*this.canvas.height/4,
            y : Math.round((y-this.canvas.height/5)/this.canvas.width/5)*this.canvas.width/5
        };

        heartShip.ship.zone[this.ship.call] = {};
        heartShip.ship.zone[this.ship.call] = {x : 'N'+x, y: 'E'+y, squareX: (opacity.x/300), squareY : (opacity.y/300), cX : x, cY : y};
        context.save();
        context.font = "bold 29px Arial";
        context.fillStyle = "#FFF";
        var stringOpacity = ''+((opacity.x/300)+1)+'-'+((opacity.y/300)+1);
        context.fillText(stringOpacity, opacity.x + opacity.width - 40, opacity.y + opacity.height);
        if(true) {
            var hUp=0;
            if(heartShip.ship.flagShip == opacity.x){
                hUp+=1;
            }
        }
        heartShip.ship.flagShip = opacity.x;
        context.font = "bold 25px Arial";
        context.fillText(this.ship.name, opacity.x, opacity.y + 30 + hUp * 30);

        context.fillStyle = 'rgba(214,220,153,.2)';
        context.fillRect(opacity.x, opacity.y, opacity.width, opacity.height);
        context.restore();

    };
}
/**
 * @name    :: GameViewStart
 * @comment :: Стартовый запуск
 */
function GameViewStart() {
    this.getName = function () {return 'viewStart'};
    this.init = function () {
        $('.maps-grid').css({
            "border": "dashed #121823 medium",
            "width": this.width + "px",
            "height": this.height + "px",
        });
    };
}
/**
 * @name    :: GameGridMaps
 * @comment :: Сетка на всей карте, рисуется отдельно
 */
function GameGridMaps(){
    this.getName = function(){return 'gridMaps'};
    this.init = function(){
        var context = this.canvas.context;
        var width   = this.canvas.width;
        var height  = this.canvas.height;

        var opacity = {
            width  : height/4,
            height : width/5
        };

        if(true) {
            context.save();
            context.beginPath();
            context.fillStyle = 'rgba(0,0,0,0.3)';
            context.font = "bold 25px Arial";
            var n = 0;

            while (n < width) {

                var h = 0;
                while (h < height) {
                    h += opacity.width;
                    context.save();
                    context.beginPath();
                    context.fillStyle = 'rgba(255,255,255, 0.8)';
                    var al = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

                    var text = al[Math.floor(n/opacity.height)]+':'+Math.floor(h/opacity.width);
                    context.fillText(text, n+opacity.width-135, h);
                    context.fill();
                    context.restore();

                }
                n += opacity.height;
                context.fillRect(n, 0, 3, height);
            }

            n = 0;
            while (n < height) {
                n += opacity.width;
                context.fillRect(0, n, width, 3);
            }

            context.fill();
            context.restore();
        }
    };
}
/**
 * @name :: GameCollision
 * @comment :: Столкновения
 */
function GameCollision(){
    this.text = 'Возможность столкновения';
    this.getName = function(){return 'collision'};
    this.init = function(){
        var preload = this.preload;
        if(preload.active.length>1){
            var ship = [];
            for(var t in preload.active){
                for(var n in preload.active){
                    if(n == t) continue;
                    var s1 = preload.active[t];
                    var s2 = preload.active[n];

                    if( (s1.x + s1.width) > (s2.x - s2.width) &&
                        (s1.x - s1.width) < (s2.x + s2.width) &&
                        (s1.y + s1.height)> (s2.y - s2.height)&&
                        (s1.y - s1.height)< (s2.y + s2.height)
                    ){
                        // @event :: событие
                        heartShip.log.add({
                            interval : 1,
                            name : this.getName(),
                            time : Math.round(new Date().getTime() / 1000),
                            text : {
                                name : this.text,
                                time : ('0' + (new Date().getHours() + 1)).slice(-2)+':'+('0' + (new Date().getMinutes() + 1)).slice(-2)+':'+('0' + (new Date().getSeconds() + 1)).slice(-2),
                                import : 'info'
                            }
                        });
                    }
                }
            }
        }
    };
}
/**
 * @name    :: GameVisibility
 * @comment :: скрывает корабли
 * @output  :: Отсутсвующие корабли
 */
function GameVisibility(){
    this.pre;
    this.getName = function(){return 'visibility'};
    this.text = 'Корабль отсутствует ::';
    this.init = function(event){
        var self = this;

        var missFull = heartShip.obj.slice();
        event.data.forEach(function(value){
            missFull.forEach(function(color, name){
                $('.'+color).show();
                if(value.color == color){
                    missFull.splice(name, 1);
                }
            });
        });

        missFull.forEach(function(name){
            var text = self.text +' '+ heartShip.color.obj[name];
            heartShip.log.add({
                interval : 5,
                name : self.getName()+name,
                time : Math.round(new Date().getTime() / 1000),
                text : {
                    name : text,
                    time : ('0' + (new Date().getHours() + 1)).slice(-2)+':'+('0' + (new Date().getMinutes() + 1)).slice(-2)+':'+('0' + (new Date().getSeconds() + 1)).slice(-2),
                    import : ''
                }
            });
            $('.'+name).hide();
        });
    };
}
/**
 * @name    :: GameRenderShip
 * @comment :: Отрисовываем карабли + некоторые события связанные с количеством появления
 */
function GameRenderShip(){
    this.getName = function(){return 'renderShip'};
    this.init = function(){
        var ship  = this.ship;
        var context = this.canvas;
        var x = this.ship.x;
        var y = this.ship.y;

        ship.render.ship.value>20 ? ship.render.ship.value = 1 : ship.render.ship.value+=1;

        ship.render.ship.ship.css({
            left : x,
            top  : y
        });

    }
}
/**
 * @name :: GamePoint
 * @comment :: Рисуем шлейф от корабля
 */

function GamePoint(){
    this.getName = function(){return 'point'};
    this.init = function(){
        var ctx = this.canvas.context;
        var color = this.ship.colorShip;
        var colorRGB = this.ship.colorShipRGB;

        heartShip.color.colors[this.ship.call].forEach(function(value, num){
            var date = value.date;
            var x = Math.round(value.x/25)*25;
            var y = Math.round(value.y/25)*25;

            ctx.beginPath();
                ctx.fillStyle = 'RGBA('+colorRGB+', 0.'+(Math.round(num/20)+1)+')';
                ctx.arc(x+25, y+25 ,  10, 0, Math.PI*2, false);
            ctx.fill();
        });
    };
}
/**
 @name : GameInfo
 @comment : Отображает инфу корабля. x/y координаты
 */

function GameInfoWinner(ship){
    $('.winner').show();
    this.shipWinner = ship;
    this.flag = 0;
    this.getName = function(){return 'winner'};
    this.init = function(){
        var self = this;
        var ship = this.shipWinner;
        var x = ship.x;
        var y = ship.y;

        $('.winner').css({
            'left' : x-100,
            'top'  : y-100
        });
        if(!this.flag) {
            this.flag=1;
            setTimeout(function () {
                self.delete(self.getName());
            }, 5000);
        }

    };
};
/**
    @name : GameInfo
    @comment : Отображает инфу корабля. x/y координаты
 */

function GameInfo(){
    this.getName = function(){return 'info'};
    this.init = function(){
        var ctx = this.canvas.context;
        var ship = this.ship;
        var x = ship.x;
        var y = ship.y;

        ctx.beginPath();
        ctx.fillStyle = "#FFF";
        var w = 60;

        ctx.fillRect(x+w, y, 50, 5);
        ctx.fill();

        ctx.font = "bold 24px Arial";
        ctx.fillText(this.ship.x, x+w, y);
        ctx.fillText(this.ship.y, x+w, y+25);

        ctx.fill();

    };
};
/**
 * @comment :: Выпадающее меню
 */
function GameDropdownInfo(time){
    this.flag = 0;

    this.time = time ? time : 30000;
    this.getName = function(){return 'dropdownInfo'};
    this.init = function(){
        var self = this;
        var ship = this.ship;
        var gameGrid = new GameGrid();
        var shift = gameGrid.dataGrid;
        ship.render.prompt.prompt.show();

        ship.render.prompt.prompt.css({
            top : ship.y + shift.height- self.canvas.height/1.7,
            left : ship.x + shift.width + self.canvas.width/2.7,
        });
        if(!this.flag){
            this.flag = 1;
            self.ship.render.prompt.func.forEach(function(value){
                value();
            });
        }
        setTimeout(function(){self.end(self)}, this.time);
    };
    this.end = function(self){
        self.delete(self.getName());

        self.ship.render.prompt.all.forEach(function(value){
            value.hide();
        });
    }
};

/**
 * @comment :: Событие переходов
 */
function GameTransition(){
    this.text = 'Перемещение';
    this.dataTransition = {};
    this.getName = function(){return 'transition'};
    this.init = function(){
        var self = this;
        gameGrid = new GameGrid();
        var opacity = {
            width  : this.canvas.width / 5,
            height : this.canvas.height / 4,
        }
        var al = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

        var num = al[Math.floor(self.ship.x/opacity.width)]+""+(Math.floor(self.ship.y/opacity.height)+1);
        if(self.dataTransition[self.ship.call] !== undefined){

            if(self.dataTransition[self.ship.call] != num){
                var text = self.text+' '+self.dataTransition[self.ship.call]+'::'+num+' :: '+self.ship.name;
                heartShip.log.add({
                    interval : 0.5,
                    name : self.getName(),
                    time : Math.round(new Date().getTime() / 1000),
                    text : {
                        name : text,
                        time : ('0' + (new Date().getHours() + 1)).slice(-2)+':'+('0' + (new Date().getMinutes() + 1)).slice(-2)+':'+('0' + (new Date().getSeconds() + 1)).slice(-2),
                        import : ''
                    }
                });
            }
        }
            self.dataTransition[self.ship.call] = num;

    }
}
/**
 * @comment :: Игра в позицию, захваатить флаг
 */
function GamePosition(){
    var flagPossition = 0;
    this.text = 'Победитель определен';
    this.arrayPossition;
    this.getName = function(){return 'position'};
    this.init = function(){
        var self = this;

        if(!flagPossition){
            var rand = (getRandomInt(0, Math.round(self.canvas.width/(self.canvas.width/5)) - 1)*(self.canvas.width/5));
            this.arrayPossition = {y : 100, x : (rand+50), w : 80, h : 150};

            document.getElementById('zone').style.top           = (self.arrayPossition.y -100) +'px';
            document.getElementById('zone').style.left          = (self.arrayPossition.x -100)+'px';
            document.getElementById('zone').style.display       = 'block';
            document.getElementById('container').style.display  = 'block';
            document.getElementById('container').style.top      = (self.arrayPossition.y + 42)+'px';
            document.getElementById('container').style.left     = (self.arrayPossition.x - 206)+'px';

        };flagPossition = 1;

        var ship = self.ship;
        var ctx = self.canvas.context;
        if(true) {
            ctx.beginPath();
            ctx.strokeStyle = 'red';
            //ctx.setLineDash([20, 20]);
            ctx.moveTo(ship.x + 60, ship.y+17);
            ctx.lineTo(this.arrayPossition.x + 40, this.arrayPossition.y + 40);
            ctx.stroke();
        }
        if((self.arrayPossition.x - self.arrayPossition.w) < ship.x && (self.arrayPossition.x + self.arrayPossition.w)+50 > ship.x && self.arrayPossition.y - (self.arrayPossition.h) < ship.y && self.arrayPossition.y + (self.arrayPossition.h) > ship.y){
            document.getElementById('zone').style.display       = 'none';
            document.getElementById('container').style.display  = 'none';
            $('#view-game').css('backgroundImage', 'url(img/v-on.png)');
            $('#view-game').attr('flag', 'off');
            var text = self.text + ' :: '+ ship.name;

            self.push(new GameInfoWinner(ship));

            heartShip.log.add({
                name : self.getName(),
                time : Math.round(new Date().getTime() / 1000),
                text : {
                    name : text,
                    time : ('0' + (new Date().getHours() + 1)).slice(-2)+':'+('0' + (new Date().getMinutes() + 1)).slice(-2)+':'+('0' + (new Date().getSeconds() + 1)).slice(-2),
                    import : 'success'
                }
            });
            // @event :: победитель
            //console.log(ship.name+' Победитель');
            var el = self.elements;
            for(var t in el){

                if(self.elements[t].getName() == 'position') {
                    self.elements.splice(t,1);
                }
            }
        }
    }
}

function getRandomInt(min, max){ return Math.floor(Math.random() * (max - min + 1)) + min;}
function inherit(derived, base, init) {
    init.prototype = base.prototype;
    derived.prototype = new init();
    derived.prototype.constructor = derived;
}
inherit(c1, Ship, Ship);
inherit(c2, Ship, Ship);
inherit(Game, Canvas, Canvas);