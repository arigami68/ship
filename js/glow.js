/*
    @name :: Радар
*/
var glow = function(context, radiusX, radiusY){
    var x = 0,
        flag = 1,
        plus = 1;
    var r = function() {
        switch(flag){
            case 1:
                context.strokeStyle='rgba(255,255,255,1)';
                if(x > 360){
                    x = 360;
                    flag=2;
                }
                x += plus;
                break;
            case 2:
                context.strokeStyle='rgba(34,141,186,1)';
                if(x < 0){
                    x = 0,
                        flag = 1;

                }
                x -= plus;
                break;
        }

        var sin = Math.sin(x / 180 * Math.PI);
        var cos = Math.cos(x / 180 * Math.PI);
        context.moveTo(radiusX, radiusY);
        context.lineTo(radiusX - (radiusX * cos), radiusY - (radiusY * sin));
    };

    setInterval(function(){
        context.beginPath();
        r();
        context.stroke();
    }, 10);
};