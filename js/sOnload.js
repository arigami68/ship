var sOnload = function(game, canvas){
    $('.btn ').popover();

    var elF = function(el){
        var flag = (el.attr('flag') === undefined) ? 'on' : el.attr('flag');
        (flag == 'on') ? el.attr('flag', 'off') : el.attr('flag', 'on');
    };

    $( "body" ).on( "click", ".yellow, .cyan", function() {
        game.push(new GameDropdownInfo(30000));
    });
    var swR = [$('.switch'), $('.switch2'), $('.switch3'), $('.switch4'), $('.switch5')];
    swR.forEach(function (value) {
        elF(value);
    });
    $('#view-game').click(function(){
        var act = {on : 'on', off : 'off'};
        var flag = $(this).attr('flag');
        flag = (flag == act.off) ? act.on : act.off;

        if(flag == act.on) {

            $(this).css('backgroundImage', 'url(img/v-off.png)');
            game.push(new GamePosition(canvas));
        }else{
            $(this).css('backgroundImage', 'url(img/v-on.png)');
            game.delete('position');
        }

        $(this).attr('flag', flag);
    });
    $('.switch, .switch2, .switch3, .switch4, .switch5').change(function () {
        var cl = $(this).attr('class');
        var fl = $(this).attr('flag');
        if(cl == 'switch5'){
            game.push(new GamePosition(canvas));
        }else if(cl == 'switch2'){

            if(fl == 'off'){
                game.push(new GameDropdownInfo(1000000));
            }else{
                game.delete('dropdownInfo');
            }

        }
        elF($(this));
    });
};